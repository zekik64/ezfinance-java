package com.ezfinance.accountservice.service;

import com.ezfinance.accountservice.model.accountList.Account;
import com.ezfinance.accountservice.model.detailed.DetailedAccount;
import com.ezfinance.accountservice.model.transactions.TransactionResponse;

/**
 * Created by Zeki on 13/03/2017.
 */
public interface AccountService {

    Account[] getAllAccounts(String authToken);

    DetailedAccount getDetailedAccountInformation(String authToken, String bankId, String accountId);

    TransactionResponse getTransactionsForAccount(String authToken, String bankId, String accountId);
}
