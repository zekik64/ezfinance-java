package com.ezfinance.accountservice.service;

import com.ezfinance.accountservice.model.accountList.Account;
import com.ezfinance.accountservice.model.detailed.DetailedAccount;
import com.ezfinance.accountservice.model.transactions.TransactionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import static org.springframework.http.HttpMethod.GET;

/**
 * Created by Zeki on 13/03/2017.
 */
@Service
public class AccountServiceImpl implements AccountService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountServiceImpl.class);

    private static String accountListUri = "http://localapi.tesobe.net:8080/obp/v2.0.0/my/accounts";
    private static String baseUri = "http://localapi.tesobe.net:8080/obp/v2.0.0/my/banks/";





    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Account[] getAllAccounts(String authToken) {

        LOGGER.info("Attempting to retrieve accounts");
        final String header = "DirectLogin token=\"" + authToken + "\"";
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
        queryParams.add("grant_type", "password");
        final HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", header);

        final HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(queryParams, headers);

        try {
            final ResponseEntity<Account[]> response = restTemplate.exchange(accountListUri, GET, request, Account[].class);
            LOGGER.info("account retrieval succeeded");
            return response.getBody();
        } catch (Exception ex) {
            LOGGER.info("Exception occurred in account retrieval: {}", ex.getMessage());
        }

        return null;

    }











    @Override
    public DetailedAccount getDetailedAccountInformation(String authToken, String bankId, String accountId) {

        final String detailedAccountUri = baseUri + bankId + "/accounts/" + accountId + "/account";

        LOGGER.info("Attempting to retrieve detailed account info");
        final String header = "DirectLogin token=\"" + authToken + "\"";
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
        queryParams.add("grant_type", "password");
        final HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", header);

        final HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(queryParams, headers);

        try {
            final ResponseEntity<DetailedAccount> response = restTemplate.exchange(detailedAccountUri, GET, request, DetailedAccount.class);
            LOGGER.info("account details retrieval succeeded");
            return response.getBody();
        } catch (Exception ex) {
            LOGGER.info("Exception occurred in account details retrieval: {}", ex.getMessage());
            //TODO:: AM Error handling rather than returning null
        }


        return null;
    }




    @Override
    public TransactionResponse getTransactionsForAccount(String authToken, String bankId, String accountId) {

        final String transactionsListUri = baseUri + bankId + "/accounts/" + accountId + "/transactions";

        LOGGER.info("Attempting to retrieve transactions");
        final String header = "DirectLogin token=\"" + authToken + "\"";
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
        queryParams.add("grant_type", "password");
        final HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", header);

        final HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(queryParams, headers);

        try {
            final ResponseEntity<TransactionResponse> response = restTemplate.exchange(transactionsListUri, GET, request, TransactionResponse.class);
            LOGGER.info("transaction retrieval succeeded");
            return response.getBody();
        } catch (Exception ex) {
            LOGGER.info("Exception occurred in transaction retrieval: {}", ex.getMessage());
            //TODO:: AM Error handling rather than returning null
        }


        return null;
    }
}
