package com.ezfinance.accountservice.model.detailed;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Zeki on 13/03/2017.
 */
public class Owner {

    @JsonProperty("id")
    private String id;
    @JsonProperty("provider")
    private String provider;
    @JsonProperty("display_name")
    private String displayName;
}
