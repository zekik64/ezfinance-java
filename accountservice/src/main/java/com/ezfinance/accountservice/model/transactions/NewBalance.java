package com.ezfinance.accountservice.model.transactions;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Zeki on 13/03/2017.
 */
public class NewBalance {

    @JsonProperty("currency")
    private String currency;
    @JsonProperty("amount")
    private String amount;
}
