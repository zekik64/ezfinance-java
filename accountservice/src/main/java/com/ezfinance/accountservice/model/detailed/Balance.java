package com.ezfinance.accountservice.model.detailed;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Zeki on 13/03/2017.
 */
public class Balance {

    @JsonProperty("currency")
    public String currency;
    @JsonProperty("amount")
    public String amount;
}
