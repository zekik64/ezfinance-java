package com.ezfinance.accountservice.model.transactions;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Zeki on 13/03/2017.
 */
public class TransactionAccount {

    @JsonProperty("id")
    private String id;
    @JsonProperty("holders")
    private List<Holder> holders = null;
    @JsonProperty("number")
    private String number;
    @JsonProperty("kind")
    private String kind;
    @JsonProperty("IBAN")
    private String iBAN;
    @JsonProperty("swift_bic")
    private Object swiftBic;
    @JsonProperty("bank")
    private Bank bank;
}
