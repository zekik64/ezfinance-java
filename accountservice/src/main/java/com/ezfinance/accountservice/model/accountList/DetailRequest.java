package com.ezfinance.accountservice.model.accountList;

/**
 * Created by Zeki on 10/05/2017.
 */
public class DetailRequest {

    public DetailRequest() {

    }

    private String authToken;
    private String bankId;
    private String accountId;

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
