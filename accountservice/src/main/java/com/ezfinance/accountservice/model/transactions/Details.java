package com.ezfinance.accountservice.model.transactions;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Zeki on 13/03/2017.
 */
public class Details {

    @JsonProperty("type")
    private String type;
    @JsonProperty("description")
    private String description;
    @JsonProperty("posted")
    private String posted;
    @JsonProperty("completed")
    private String completed;
    @JsonProperty("new_balance")
    private NewBalance newBalance;
    @JsonProperty("value")
    private Value value;
}
