package com.ezfinance.accountservice.model.transactions;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Transaction {

    @JsonProperty("id")
    private String id;

    @JsonProperty("account")
    private TransactionAccount account;

    @JsonProperty("counterparty")
    private TransactionAccount counterparty;

    @JsonProperty("details")
    private Details details;
}
