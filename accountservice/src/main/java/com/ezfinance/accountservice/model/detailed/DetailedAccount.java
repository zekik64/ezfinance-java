package com.ezfinance.accountservice.model.detailed;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Zeki on 13/03/2017.
 */
public class DetailedAccount {

    @JsonProperty("id")
    public String id;
    @JsonProperty("label")
    public String label;
    @JsonProperty("number")
    public String number;
    @JsonProperty("owners")
    public List<Owner> owners = null;
    @JsonProperty("type")
    public String type;
    @JsonProperty("balance")
    public Balance balance;
    @JsonProperty("IBAN")
    public String iBAN;
    @JsonProperty("swift_bic")
    public Object swiftBic;
    @JsonProperty("bank_id")
    public String bankId;
}
