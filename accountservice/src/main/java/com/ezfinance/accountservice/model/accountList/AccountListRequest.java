package com.ezfinance.accountservice.model.accountList;

public class AccountListRequest {

    public AccountListRequest() {

    }

    private String authToken;

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

}
