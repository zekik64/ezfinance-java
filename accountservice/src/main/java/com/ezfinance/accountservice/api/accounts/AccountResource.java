package com.ezfinance.accountservice.api.accounts;

import com.ezfinance.accountservice.model.accountList.AccountListRequest;
import com.ezfinance.accountservice.model.accountList.DetailRequest;
import com.ezfinance.accountservice.model.detailed.DetailedAccount;
import com.ezfinance.accountservice.model.accountList.Account;
import com.ezfinance.accountservice.model.transactions.Transaction;
import com.ezfinance.accountservice.model.transactions.TransactionResponse;
import com.ezfinance.accountservice.service.AccountService;
import com.ezfinance.accountservice.service.AccountServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/account")
@CrossOrigin()
public class AccountResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountResource.class);


    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/accountList", method = POST)
    public Account[] retrieveAccountList(
            @RequestBody AccountListRequest accountListRequest, HttpServletResponse response)
            throws Exception {

       return accountService.getAllAccounts(accountListRequest.getAuthToken());
    }

    @RequestMapping(value = "/transaction", method = POST)
    public Transaction[] retrieveAccountTransactions(@RequestBody DetailRequest DetailRequest) {

        return accountService.getTransactionsForAccount(DetailRequest.getAuthToken(), DetailRequest.getBankId(), DetailRequest.getAccountId()).getTransactions();
    }

    @RequestMapping(value = "/detail", method = POST)
    public DetailedAccount retrieveDetailedAccount(@RequestBody DetailRequest DetailRequest) {

        return accountService.getDetailedAccountInformation(DetailRequest.getAuthToken(), DetailRequest.getBankId(), DetailRequest.getAccountId());
    }
}
