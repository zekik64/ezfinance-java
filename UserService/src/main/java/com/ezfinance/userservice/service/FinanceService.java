package com.ezfinance.userservice.service;


import com.ezfinance.userservice.model.DisposableIncome;
import com.ezfinance.userservice.model.FinancialPrediction;
import com.ezfinance.userservice.model.RegularExpenditure;
import com.ezfinance.userservice.model.RegularIncome;
import com.ezfinance.userservice.model.transactions.Transaction;

import java.util.ArrayList;

public interface FinanceService {

    void saveRegularIncome(RegularIncome regularIncome);

    void updateRegularIncome(RegularIncome regularIncome);

    void deleteRegularIncome(RegularIncome regularIncome);

    ArrayList<RegularIncome> retrieveAllRegularIncomes(Long userid);




    void saveRegularExpenditure(RegularExpenditure regularExpenditure);

    void updateRegularExpenditure(RegularExpenditure regularExpenditure);

    void deleteRegularExpenditure(RegularExpenditure regularExpenditure);

    ArrayList<RegularExpenditure> retrieveAllRegularExpenditures(Long userid);



    FinancialPrediction generateFinancialPrediction(Long userid, double currentBalance, Transaction[] transactions);

    void generateFinancialAdvice();

    RegularIncome calculateNextIncome(ArrayList<RegularIncome> regularIncomes);

    double calculateExpendituresComingOutBeforeNextIncome(ArrayList<RegularExpenditure> regularExpenditures, int nextIncomeDate);

    DisposableIncome calculateDisposableIncome(Long userid, ArrayList<RegularExpenditure> regularExpenditures, ArrayList<RegularIncome> regularIncomes);

    double calculateTotalTransactionAmount(Transaction[] transactions);
}
