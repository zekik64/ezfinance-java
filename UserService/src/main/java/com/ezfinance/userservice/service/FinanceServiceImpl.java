package com.ezfinance.userservice.service;

import com.ezfinance.userservice.dao.FinancialAdviceRepository;
import com.ezfinance.userservice.dao.RegularExpenditureRepository;
import com.ezfinance.userservice.dao.RegularIncomeRepository;
import com.ezfinance.userservice.model.DisposableIncome;
import com.ezfinance.userservice.model.FinancialPrediction;
import com.ezfinance.userservice.model.RegularExpenditure;
import com.ezfinance.userservice.model.RegularIncome;
import com.ezfinance.userservice.model.transactions.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class FinanceServiceImpl implements FinanceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FinanceServiceImpl.class);


    @Autowired
    FinancialAdviceRepository financialAdviceRepository;

    @Autowired
    RegularExpenditureRepository regularExpenditureRepository;

    @Autowired
    RegularIncomeRepository regularIncomeRepository;



    @Override
    public void saveRegularIncome(RegularIncome regularIncome) {
        regularIncomeRepository.save(regularIncome);
    }

    @Override
    public void updateRegularIncome(RegularIncome regularIncome) {

        regularIncomeRepository.save(regularIncome);

    }

    @Override
    public void deleteRegularIncome(RegularIncome regularIncome) {
        regularIncomeRepository.delete(regularIncome);
    }

    @Override
    public ArrayList<RegularIncome> retrieveAllRegularIncomes(Long userid) {
        return regularIncomeRepository.findAllRegularIncomes(userid);
    }

    @Override
    public void saveRegularExpenditure(RegularExpenditure regularExpenditure) {

        regularExpenditureRepository.save(regularExpenditure);

    }

    @Override
    public void updateRegularExpenditure(RegularExpenditure regularExpenditure) {

        regularExpenditureRepository.save(regularExpenditure);

    }

    @Override
    public void deleteRegularExpenditure(RegularExpenditure regularExpenditure) {
        regularExpenditureRepository.delete(regularExpenditure);
    }

    @Override
    public ArrayList<RegularExpenditure> retrieveAllRegularExpenditures(Long userid) {

        return regularExpenditureRepository.findAllRegularExpenditures(userid);
    }

    @Override
    public FinancialPrediction generateFinancialPrediction(Long userid, double currentBalance, Transaction[] transactions) {


        FinancialPrediction financialPrediction = new FinancialPrediction();


        // Get the current date and time
        LocalDateTime currentTime = LocalDateTime.now();


        int currentDay = currentTime.getDayOfMonth();
        LOGGER.info("Current day is: " + currentDay);
        financialPrediction.setTodaysDate(currentTime.toLocalDate());


        //get average daily spend so far.
        double totalTransactionAmountForMonth = calculateTotalTransactionAmount(transactions);

        LOGGER.info("average daily spend is: " + (totalTransactionAmountForMonth / currentDay));
        double averageDailySpend = (totalTransactionAmountForMonth / currentDay);


        financialPrediction.setAverageAmountSpentPerDay(averageDailySpend);
        financialPrediction.setTotalAmountSpentThisMonth(totalTransactionAmountForMonth);

        // retrieve expenditures
        ArrayList<RegularExpenditure> regularExpenditures = retrieveAllRegularExpenditures(userid);
        //retrieve incomes
        ArrayList<RegularIncome> regularIncomes = retrieveAllRegularIncomes(userid);

        //check many days until next income
        RegularIncome nextIncome = calculateNextIncome(regularIncomes);
        int daysUntilNextIncome = nextIncome.getDayofmonth() - currentDay;
        financialPrediction.setNextIncomeAmount(nextIncome.getAmount());
        financialPrediction.setNextIncomeDate(nextIncome.getDayofmonth());
        financialPrediction.setNextIncomeDescription(nextIncome.getDescription());

        //calculate expenditures coming out before next income
        double expendituresComingOutBeforeNextIncome = calculateExpendituresComingOutBeforeNextIncome(
                regularExpenditures, nextIncome.getDayofmonth());
        financialPrediction.setExpendituresBeforeNextIncome(expendituresComingOutBeforeNextIncome);


        //check if enough money to last
        double predictedAmountToBeSpent = expendituresComingOutBeforeNextIncome + (averageDailySpend * daysUntilNextIncome);

        financialPrediction.setPredictedAmountToBeSpent(predictedAmountToBeSpent);
        //calculate disposable income info
        financialPrediction.setDisposableIncome(calculateDisposableIncome(userid, regularExpenditures, regularIncomes));

        if (predictedAmountToBeSpent > currentBalance) {
            LOGGER.info("YOU WONT HAVE ENOUGH MONEY");
            financialPrediction.setWillHaveEnoughMoney(false);
        } else {
            LOGGER.info("YOU WILL HAVE ENOUGH MONEY");
            financialPrediction.setWillHaveEnoughMoney(true);
        }
        return financialPrediction;
    }

    @Override
    public void generateFinancialAdvice() {

        //based on prediction generate advice to user e.g. save more money, or spend such and such per day.

    }


    @Override
    public RegularIncome calculateNextIncome(ArrayList<RegularIncome> regularIncomes) {

        int nextIncomeDate = 99;
        int indexOfIncomeToReturn = 0;

        //get current day of month
        LocalDateTime currentTime = LocalDateTime.now();
        int currentDay = currentTime.getDayOfMonth();
        LOGGER.info("Current day is: " + currentDay);


        //loop through incomes to find the next one that is coming in
        for (int i = 0; i < regularIncomes.size(); i++) {

            if(regularIncomes.get(i).getDayofmonth() > (currentDay) && (regularIncomes.get(i).getDayofmonth() < nextIncomeDate) ){
                LOGGER.info(regularIncomes.get(i).getDayofmonth() + "is greater than " + currentDay);
                LOGGER.info(regularIncomes.get(i).getDayofmonth() + "is less than " + nextIncomeDate);

                nextIncomeDate = regularIncomes.get(i).getDayofmonth();
                indexOfIncomeToReturn = i;
            }
        }

        return regularIncomes.get(indexOfIncomeToReturn);
    }

    @Override
    public double calculateExpendituresComingOutBeforeNextIncome(ArrayList<RegularExpenditure> regularExpenditures, int nextIncomeDate) {

        double expendituresComingOutBeforeNextIncome = 0;

        for (int i = 0; i < regularExpenditures.size(); i++) {

            if(regularExpenditures.get(i).getDayofmonth() < (nextIncomeDate)){
                expendituresComingOutBeforeNextIncome += regularExpenditures.get(i).getAmount();
            }
        }

        return expendituresComingOutBeforeNextIncome;
    }




    @Override
    public DisposableIncome calculateDisposableIncome(Long userid, ArrayList<RegularExpenditure> regularExpenditures, ArrayList<RegularIncome> regularIncomes) {

        DisposableIncome disposableIncome = new DisposableIncome();
        //get disposable income - total of incomes minus total of expenditures
        double totalIncome = 0;
        for (int i = 0; i < regularIncomes.size(); i++) {
            totalIncome += regularIncomes.get(i).getAmount();
        }
        disposableIncome.setTotalMonthlyIncome(totalIncome);

        double totalExpenditure = 0;
        for (int i = 0; i < regularIncomes.size(); i++) {
            totalExpenditure += regularExpenditures.get(i).getAmount();
        }
        disposableIncome.setTotalMonthlyExpenditures(totalExpenditure);

        disposableIncome.setDisposableIncome(totalIncome - totalExpenditure);

        return disposableIncome;
    }






    @Override
    public double calculateTotalTransactionAmount(Transaction[] transactions) {

        double totalTransactionAmountForMonth = 0;

        for (int i = 0; i < transactions.length; i++) {

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            Calendar cal = Calendar.getInstance();

            try {
                Date transactionDate=format.parse(transactions[i].getDetails().getCompleted());
                cal.setTime(transactionDate);
                int transactionMonth = cal.get(Calendar.MONTH);
                int transactionYear = cal.get(Calendar.YEAR);

                cal.setTime(new Date());
                int currentMonth = cal.get(Calendar.MONTH);
                int currentYear = cal.get(Calendar.YEAR);

                if ((transactionMonth == currentMonth) && (transactionYear == currentYear) && (transactions[i].getDetails().getValue().getAmount() > 0)) {
                    totalTransactionAmountForMonth +=  transactions[i].getDetails().getValue().getAmount();
                } else if((transactionMonth == currentMonth) && (transactionYear == currentYear) && (transactions[i].getDetails().getValue().getAmount() < 0)) {
                    totalTransactionAmountForMonth -=  transactions[i].getDetails().getValue().getAmount();
                }

                LOGGER.info("total transaction amount is:" + totalTransactionAmountForMonth);



            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return totalTransactionAmountForMonth;
    }
}
