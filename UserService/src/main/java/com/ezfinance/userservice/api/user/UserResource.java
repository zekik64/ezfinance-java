package com.ezfinance.userservice.api.user;

import com.ezfinance.userservice.model.*;
import com.ezfinance.userservice.service.FinanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/user")
@CrossOrigin()
public class UserResource {

    @Autowired
    FinanceService financeService;

    @RequestMapping(value = "/addincome", method = POST)
    public void addRegularIncome(@RequestBody RegularIncome regularIncome) {
        financeService.saveRegularIncome(regularIncome);
    }

    @RequestMapping(value = "/updateincome", method = POST)
    public void updateRegularIncome(@RequestBody RegularIncome regularIncome) {
        financeService.updateRegularIncome(regularIncome);
    }

    @RequestMapping(value = "/deleteincome", method = POST)
    public void removeRegularIncome(@RequestBody RegularIncome regularIncome) {
        financeService.deleteRegularIncome(regularIncome);
    }

    @RequestMapping(value = "/incomes", method = POST)
    public ArrayList<RegularIncome> getAllIncomes(@RequestBody RegularRequest regularRequest) {
        return financeService.retrieveAllRegularIncomes(regularRequest.getUserId());
    }






    @RequestMapping(value = "/addexpenditure", method = POST)
    public void addRegularExpenditure(@RequestBody RegularExpenditure regularExpenditure) {
        financeService.saveRegularExpenditure(regularExpenditure);
    }

    @RequestMapping(value = "/updateexpenditure", method = POST)
    public void updateRegularExpenditure(@RequestBody RegularExpenditure regularExpenditure) {
        financeService.updateRegularExpenditure(regularExpenditure);
    }

    @RequestMapping(value = "/deleteexpenditure", method = POST)
    public void removeRegularExpenditure(@RequestBody RegularExpenditure regularExpenditure) {
        financeService.deleteRegularExpenditure(regularExpenditure);
    }

    @RequestMapping(value = "/expenditures", method = POST)
    public ArrayList<RegularExpenditure> getAllExpenditures(@RequestBody RegularRequest regularRequest) {

        return financeService.retrieveAllRegularExpenditures(regularRequest.getUserId());
    }







    @RequestMapping(value = "/generatePrediction", method = POST)
    public FinancialPrediction generateFinancialPrediction(@RequestBody FinancialPredictionRequest financialPredictionRequest) {
        return financeService.generateFinancialPrediction(financialPredictionRequest.getUserid(),
                financialPredictionRequest.getCurrentbalance(), financialPredictionRequest.getTransactions());
    }






    @RequestMapping(value = "/generateAdvice", method = POST)
    public void generateFinancialAdvice() {
        financeService.generateFinancialAdvice();
    }

}
