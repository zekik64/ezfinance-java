package com.ezfinance.userservice.dao;

import com.ezfinance.userservice.model.RegularExpenditure;
import com.ezfinance.userservice.model.RegularIncome;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;

public interface RegularExpenditureRepository extends CrudRepository<RegularExpenditure, Long> {

    @Query(value = "SELECT p FROM RegularExpenditure p WHERE p.userid = ?1  order by p.id")
    ArrayList<RegularExpenditure> findAllRegularExpenditures(Long userid);
}
