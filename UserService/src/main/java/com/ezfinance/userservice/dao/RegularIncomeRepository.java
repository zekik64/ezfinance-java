package com.ezfinance.userservice.dao;

import com.ezfinance.userservice.model.RegularIncome;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;

public interface RegularIncomeRepository extends CrudRepository<RegularIncome, Long> {

    @Query(value = "SELECT p FROM RegularIncome p WHERE p.userid = ?1  order by p.id")
    ArrayList<RegularIncome> findAllRegularIncomes(Long userid);
}
