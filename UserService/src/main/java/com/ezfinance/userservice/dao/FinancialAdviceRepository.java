package com.ezfinance.userservice.dao;

import com.ezfinance.userservice.model.FinancialAdvice;
import org.springframework.data.repository.CrudRepository;


public interface FinancialAdviceRepository extends CrudRepository<FinancialAdvice, Long> {
}
