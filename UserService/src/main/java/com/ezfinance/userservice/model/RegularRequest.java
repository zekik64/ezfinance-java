package com.ezfinance.userservice.model;

/**
 * Created by Zeki on 11/05/2017.
 */
public class RegularRequest {

    public RegularRequest() {

    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    private Long userId;
}
