package com.ezfinance.userservice.model.transactions;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Zeki on 13/03/2017.
 */
public class Holder {

    @JsonProperty("name")
    private String name;
    @JsonProperty("is_alias")
    private Boolean isAlias;
}
