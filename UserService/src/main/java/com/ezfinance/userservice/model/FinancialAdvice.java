package com.ezfinance.userservice.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import static javax.persistence.GenerationType.AUTO;

@Entity(name = "FinancialAdvice")
public class FinancialAdvice {

  private Long userid;

  @Id
  @GeneratedValue(strategy = AUTO)
  private Long adviceid;

  private String advicetext;

  public Long getUserid() {
    return userid;
  }

  public void setUserid(Long userid) {
    this.userid = userid;
  }

  public Long getAdviceid() {
    return adviceid;
  }

  public void setAdviceid(Long adviceid) {
    this.adviceid = adviceid;
  }

  public String getAdvicetext() {
    return advicetext;
  }

  public void setAdvicetext(String advicetext) {
    this.advicetext = advicetext;
  }
}
