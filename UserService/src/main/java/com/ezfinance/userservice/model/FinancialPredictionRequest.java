package com.ezfinance.userservice.model;

import com.ezfinance.userservice.model.transactions.Transaction;

/**
 * Created by Zeki on 13/05/2017.
 */
public class FinancialPredictionRequest {

    private Long userid;

    private Double currentbalance;

    private Transaction[] transactions;


    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public Double getCurrentbalance() {
        return currentbalance;
    }

    public void setCurrentbalance(Double currentbalance) {
        this.currentbalance = currentbalance;
    }

    public Transaction[] getTransactions() {
        return transactions;
    }

    public void setTransactions(Transaction[] transactions) {
        this.transactions = transactions;
    }
}
