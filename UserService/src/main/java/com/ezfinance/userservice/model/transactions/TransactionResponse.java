package com.ezfinance.userservice.model.transactions;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Zeki on 13/03/2017.
 */
public class TransactionResponse {

    public Transaction[] getTransactions() {
        return transactions;
    }

    public void setTransactions(Transaction[] transactions) {
        this.transactions = transactions;
    }

    @JsonProperty("transactions")
    private Transaction[] transactions;
}
