package com.ezfinance.userservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import static javax.persistence.GenerationType.AUTO;

@Entity(name = "RegularIncome")
public class RegularIncome {

  @JsonProperty("userId")
  private Long userid;

  @Id
  @GeneratedValue(strategy = AUTO)
  private Long id;

  private Double amount;
  private String frequency;
  private int dayofmonth;
  private String type;
  private String description;


  public int getDayofmonth() {
    return dayofmonth;
  }

  public void setDayofmonth(int dayofmonth) {
    this.dayofmonth = dayofmonth;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }



  public  String getType() {
    return type;
  }

  public  void setType(String type) {
    this.type = type;
  }


  public Long getUserid() {
    return userid;
  }

  public void setUserid(Long userid) {
    this.userid = userid;
  }

  public Long getId() {
    return id;
  }

  public void getId(Long incomeid) {
    this.id = incomeid;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public String getFrequency() {
    return frequency;
  }

  public void setFrequency(String frequency) {
    this.frequency = frequency;
  }
}
