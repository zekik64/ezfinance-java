package com.ezfinance.userservice.model;

/**
 * Created by Zeki on 13/05/2017.
 */
public class DisposableIncome {

    private double disposableIncome;
    private double totalMonthlyIncome;
    private double totalMonthlyExpenditures;

    public double getDisposableIncome() {
        return disposableIncome;
    }

    public void setDisposableIncome(double disposableIncome) {
        this.disposableIncome = disposableIncome;
    }

    public double getTotalMonthlyIncome() {
        return totalMonthlyIncome;
    }

    public void setTotalMonthlyIncome(double totalMonthlyIncome) {
        this.totalMonthlyIncome = totalMonthlyIncome;
    }

    public double getTotalMonthlyExpenditures() {
        return totalMonthlyExpenditures;
    }

    public void setTotalMonthlyExpenditures(double totalMonthlyExpenditures) {
        this.totalMonthlyExpenditures = totalMonthlyExpenditures;
    }

}
