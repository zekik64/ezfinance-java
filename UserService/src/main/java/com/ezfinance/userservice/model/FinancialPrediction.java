package com.ezfinance.userservice.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import java.time.LocalDate;
import java.util.Date;

import static javax.persistence.GenerationType.AUTO;

public class FinancialPrediction {



  private Boolean willHaveEnoughMoney;
  private double predictedAmountToBeSpent;
  private double expendituresBeforeNextIncome;
  private int nextIncomeDate;
  private double nextIncomeAmount;
  private String nextIncomeDescription;
  private LocalDate todaysDate;
  private double averageAmountSpentPerDay;
  private double totalAmountSpentThisMonth;
  private DisposableIncome disposableIncome;


  public DisposableIncome getDisposableIncome() {
    return disposableIncome;
  }

  public void setDisposableIncome(DisposableIncome disposableIncome) {
    this.disposableIncome = disposableIncome;
  }



  public Boolean getWillHaveEnoughMoney() {
    return willHaveEnoughMoney;
  }

  public void setWillHaveEnoughMoney(Boolean willHaveEnoughMoney) {
    this.willHaveEnoughMoney = willHaveEnoughMoney;
  }

  public double getPredictedAmountToBeSpent() {
    return predictedAmountToBeSpent;
  }

  public void setPredictedAmountToBeSpent(double predictedAmountToBeSpent) {
    this.predictedAmountToBeSpent = predictedAmountToBeSpent;
  }

  public double getExpendituresBeforeNextIncome() {
    return expendituresBeforeNextIncome;
  }

  public void setExpendituresBeforeNextIncome(double expendituresBeforeNextIncome) {
    this.expendituresBeforeNextIncome = expendituresBeforeNextIncome;
  }

  public int getNextIncomeDate() {
    return nextIncomeDate;
  }

  public void setNextIncomeDate(int nextIncomeDate) {
    this.nextIncomeDate = nextIncomeDate;
  }

  public double getNextIncomeAmount() {
    return nextIncomeAmount;
  }

  public void setNextIncomeAmount(double nextIncomeAmount) {
    this.nextIncomeAmount = nextIncomeAmount;
  }

  public LocalDate getTodaysDate() {
    return todaysDate;
  }

  public void setTodaysDate(LocalDate todaysDate) {
    this.todaysDate = todaysDate;
  }

  public double getAverageAmountSpentPerDay() {
    return averageAmountSpentPerDay;
  }

  public void setAverageAmountSpentPerDay(double averageAmountSpentPerDay) {
    this.averageAmountSpentPerDay = averageAmountSpentPerDay;
  }

  public double getTotalAmountSpentThisMonth() {
    return totalAmountSpentThisMonth;
  }

  public void setTotalAmountSpentThisMonth(double totalAmountSpentThisMonth) {
    this.totalAmountSpentThisMonth = totalAmountSpentThisMonth;
  }


  public String getNextIncomeDescription() {
    return nextIncomeDescription;
  }

  public void setNextIncomeDescription(String nextIncomeDescription) {
    this.nextIncomeDescription = nextIncomeDescription;
  }

}
