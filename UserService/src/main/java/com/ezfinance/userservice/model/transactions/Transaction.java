package com.ezfinance.userservice.model.transactions;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Transaction {

    @JsonProperty("id")
    private String id;

    @JsonProperty("account")
    private TransactionAccount account;

    @JsonProperty("counterparty")
    private TransactionAccount counterparty;

    @JsonProperty("details")
    private Details details;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TransactionAccount getAccount() {
        return account;
    }

    public void setAccount(TransactionAccount account) {
        this.account = account;
    }

    public TransactionAccount getCounterparty() {
        return counterparty;
    }

    public void setCounterparty(TransactionAccount counterparty) {
        this.counterparty = counterparty;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }
}
