package com.ezfinance.authenticationservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Zeki on 13/03/2017.
 */
public class OBPToken {

    @JsonProperty("token")
    private String tokenString;

    @JsonProperty("token")
    public String getToken() {
        return tokenString;
    }

    @JsonProperty("token")
    public void setToken(String token) {
        this.tokenString = token;
    }

}
