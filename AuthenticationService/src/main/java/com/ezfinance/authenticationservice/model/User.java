package com.ezfinance.authenticationservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import static javax.persistence.GenerationType.AUTO;

@Entity(name = "User")
public class User {

  public User(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  public User() {

  }

  @Id
  @GeneratedValue(strategy = AUTO)
  private Long userid;

  @JsonProperty("firstname")
  private String firstname;

  @JsonProperty("surname")
  private String surname;


  @JsonProperty("emailAddress")
  private String emailAddress;

  @JsonProperty("dob")
  private java.sql.Timestamp dob;



  public String getEmailaddress() {
    return emailAddress;
  }

  public void setEmailaddress(String emailaddress) {
    this.emailAddress = emailaddress;
  }

  public Long getUserid() {
    return userid;
  }

  public void setUserid(Long userid) {
    this.userid = userid;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public java.sql.Timestamp getDob() {
    return dob;
  }

  public void setDob(java.sql.Timestamp dob) {
    this.dob = dob;
  }
}
