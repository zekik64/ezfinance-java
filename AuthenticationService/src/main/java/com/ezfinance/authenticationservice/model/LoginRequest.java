package com.ezfinance.authenticationservice.model;

/**
 * Created by Zeki on 10/05/2017.
 */
public class LoginRequest {

    public LoginRequest() {

    }

    private String username;

    private String password;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
