/**
 * STANDARD BANNER
 */
package com.ezfinance.authenticationservice.service;

public interface AuthService {

    /**
     * Log a user in for a given username and password
     *
     * @param username The username
     * @param password The password
     * @return An authentication token if login was successful
     */
    String authenticate(String username, String password);
}
