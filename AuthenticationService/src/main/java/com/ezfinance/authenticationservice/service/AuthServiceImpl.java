package com.ezfinance.authenticationservice.service;

import com.ezfinance.authenticationservice.model.OBPToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Zeki on 13/03/2017.
 */

@Service
public class AuthServiceImpl implements AuthService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthServiceImpl.class);

    //TODO: EXTRACT INTO YAML FILE
    private static String consumerKey = "zdndcvtjdm2zx315dixujgfo2sx4g3xwmhpenexw";

    private static String accessTokenUri = "http://localapi.tesobe.net:8080/my/logins/direct";

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public String authenticate(String username, String password) {

        LOGGER.info("Attempting to authenticate user: {}", username);

        final String header = "DirectLogin username=\"" + username + "\",password=\"" + password + "\", consumer_key=\"" + consumerKey + "\"";

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
        queryParams.add("grant_type", "password");

        final HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", header);

        final HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(queryParams, headers);


        try {
            final OBPToken result = restTemplate.postForObject(accessTokenUri, request, OBPToken.class);
            LOGGER.info("Authentication succeeded");
            return result.getToken();
        } catch (Exception ex) {
            LOGGER.info("Exception occurred in Authentication: {}", ex.getMessage());
        }

        return null;
    }
}
