package com.ezfinance.authenticationservice.service;


import com.ezfinance.authenticationservice.dao.UserRepository;
import com.ezfinance.authenticationservice.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SignupServiceImpl implements SignupService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public void registerUser(User user) {

        userRepository.save(user);
    }

    @Override
    public Long findUserId(String emailAddress) {
        return userRepository.findByEmailAddress(emailAddress).getUserid();
    }
}
