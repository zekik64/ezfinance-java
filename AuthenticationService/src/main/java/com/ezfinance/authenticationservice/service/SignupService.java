package com.ezfinance.authenticationservice.service;


import com.ezfinance.authenticationservice.model.User;

/**
 * Created by Zeki on 08/05/2017.
 */
public interface SignupService {

    void registerUser(User user);

    Long findUserId(String emailAddress);
}
