package com.ezfinance.authenticationservice.api.authentication;

import com.ezfinance.authenticationservice.model.LoginRequest;
import com.ezfinance.authenticationservice.model.User;
import com.ezfinance.authenticationservice.service.AuthService;
import com.ezfinance.authenticationservice.service.SignupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

import static javax.servlet.http.HttpServletResponse.SC_OK;
import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by Zeki on 13/03/2017.
 */

@RestController
@RequestMapping("/oauth2")
@CrossOrigin()
public class AuthResource {

    @Autowired
    private AuthService authService;

    @Autowired
    private SignupService signupService;

    @RequestMapping(value = "/access_token", method = POST)
    public void authenticateUser(@RequestBody LoginRequest loginRequest,
                                 HttpServletResponse response) throws Exception {

        final String accessToken = authService.authenticate(loginRequest.getUsername(), loginRequest.getPassword());

        if (accessToken == null || accessToken.isEmpty()) {
            response.setStatus(SC_UNAUTHORIZED);
        } else {
            try {
                signupService.findUserId(loginRequest.getUsername());
            } catch (NullPointerException exception) {
                signupService.registerUser(new User(loginRequest.getUsername()));
            } finally {
                response.addHeader("Access-Control-Expose-Headers", "X-AUTH-TOKEN");
                response.setHeader("X-AUTH-TOKEN", accessToken);
                response.addHeader("Access-Control-Expose-Headers", "X-USER-ID");
                response.setHeader("X-USER-ID", String.valueOf(signupService.findUserId(loginRequest.getUsername())));
                response.setStatus(SC_OK);
            }
        }

    }
}
