package com.ezfinance.authenticationservice.dao;

import com.ezfinance.authenticationservice.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

    User findByEmailAddress(String EmailAddress);

}
